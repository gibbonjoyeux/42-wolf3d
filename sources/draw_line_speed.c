#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw_line(t_wolf *wolf,
								double height,
								t_color color_base,
								t_color color_line,
								uint16_t x)
{
	int32_t		y;
	double		max_speed;

	if (wolf->speed_up > 0)
		max_speed = GAME_MAX_SPEED * (1 + (wolf->speed_up / GAME_SPEED_UP_TIME)
		* GAME_SPEED_UP_COEF);
	else
		max_speed = GAME_MAX_SPEED;
	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_base.r, color_base.g,
	color_base.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, MAX(0, y), x,
	MIN(WINDOW_H, y + height));
	height *= 0.05 + 0.7 - (wolf->speed / max_speed) * 0.7;
	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_line.r, color_line.g,
	color_line.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + height);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_line_speed(t_wolf *wolf,
								double dist,
								uint16_t x)
{
	double		height;
	t_color		color_base;
	t_color		color_line;
	uint16_t	min_y;

	get_color(&color_base, 2, 1);
	if (wolf->speed_up > 0)
		get_color(&color_line, 1, 1);
	else
		get_color(&color_line, 3, 1);
	height = WINDOW_H / dist;
	min_y = ABS((WINDOW_H - height) * 0.5);
	draw_line(wolf, height, color_base, color_line, x);
}
