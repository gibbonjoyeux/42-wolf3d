#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static uint8_t	**get_map(int32_t file, uint16_t *w, uint16_t *h)
{
	uint8_t			**map;
	uint8_t			*line;

	map = NULL;
	*h = 0;
	*w = 0;
	while ((line = load_map_line(file, w)))
	{
		(*h) += 1;
		if (!(map = reallocf(map, sizeof(uint8_t*) * (*h + 2))))
			ct_exit_fail("Not enough memory to load the map.");
		map[*h] = line;
	}
	if (!*h)
		ct_exit_fail("Empty map.");
	if (!(map[0] = malloc((*w + 2) * sizeof(uint8_t))))
		ct_exit_fail("Not enough memory to load the map.");
	if (!(map[*h + 1] = malloc((*w + 2) * sizeof(uint8_t))))
		ct_exit_fail("Not enough memory to load the map.");
	ct_memset(map[*h + 1], GAME_TILE_SOLID, *w + 2);
	ct_memset(map[0], GAME_TILE_SOLID, *w + 2);
	return (map);
}

static void		get_player_info(t_wolf *wolf, int32_t file)
{
	char		**split;
	char		*string;
	int32_t		ret;

	if ((ret = get_next_line(file, &string)) < 0)
		ct_exit_fail("An error occured while reading the map.");
	if (!ret)
		ct_exit_fail("Player info are missing.");
	if (!(split = ct_strsplitwhite(string)))
		ct_exit_fail("Not enough memory to load the map.");
	free(string);
	if (ct_strtablen(split) != 3)
		ct_exit_fail("Player info are missing.");
	wolf->pos.x = ct_atoi(split[0]) + 1;
	wolf->pos.y = ct_atoi(split[1]) + 1;
	wolf->angle = 45 * ct_atoi(split[2]);
}

static void	check_player(t_wolf *wolf)
{
	if (wolf->pos.x >= wolf->map_w - 1
	|| wolf->pos.x < 0
	|| wolf->pos.y >= wolf->map_h - 1
	|| wolf->pos.y < 0)
		ct_exit_fail("Player is placed out of the map.");
	if (wolf->map[(int32_t)wolf->pos.y][(int32_t)wolf->pos.x] >= GAME_TILE_SOLID)
		ct_exit_fail("Player is placed into a wall.");
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void				load_map(t_wolf *wolf, char *file_name)
{
	int32_t		file;
	uint8_t		**map;

	if (!file_name)
	{
		file = open(PATH_DEFAULT_RACE, O_RDONLY);
		wolf->race_path = ct_strdup(PATH_DEFAULT_RACE);
	}
	else
	{
		file = open(file_name, O_RDONLY);
		wolf->race_path = ct_strdup(file_name);
	}
	if (!wolf->race_path)
		ct_exit_fail("Not enough memory to save race path.");
	if (file < 0)
		ct_exit_fail("Cannot open race file.");
	get_player_info(wolf, file);
	map = get_map(file, &(wolf->map_w), &(wolf->map_h));
	if (!wolf->map_w || !wolf->map_h)
		ct_exit_fail("Map does not have correct size.");
	close(file);
	wolf->map_w += 2;
	wolf->map_h += 2;
	wolf->map = map;
	check_player(wolf);
}
