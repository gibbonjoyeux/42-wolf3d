#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static inline void	put_point(t_wolf *wolf, t_vec loop)
{
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + loop.x, MINIMAP_Y + loop.y);
}

static inline void	draw_point(t_wolf *wolf, t_vec coords, t_vec loop)
{
	if (wolf->map[coords.y][coords.x] == 0)
		put_point(wolf, loop);
	else if (wolf->map[coords.y][coords.x] < GAME_TILE_SOLID)
	{
		set_color(wolf, 3);
		put_point(wolf, loop);
		set_color(wolf, 1);
	}
	else if (wolf->map[coords.y][coords.x] == GAME_TILE_END)
	{
		set_color(wolf, 4);
		put_point(wolf, loop);
		set_color(wolf, 1);
	}
}

static void			draw_center(t_wolf *wolf)
{
	set_color(wolf, 3);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2,
	MINIMAP_Y + MINIMAP_H / 2);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2 - 1,
	MINIMAP_Y + MINIMAP_H / 2);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2 + 1,
	MINIMAP_Y + MINIMAP_H / 2);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2,
	MINIMAP_Y + MINIMAP_H / 2 - 1);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2,
	MINIMAP_Y + MINIMAP_H / 2 + 1);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2 - 1,
	MINIMAP_Y + MINIMAP_H / 2 - 1);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2 - 1,
	MINIMAP_Y + MINIMAP_H / 2 + 1);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2 + 1,
	MINIMAP_Y + MINIMAP_H / 2 - 1);
	SDL_RenderDrawPoint(wolf->sdl.rend, MINIMAP_X + MINIMAP_W / 2 + 1,
	MINIMAP_Y + MINIMAP_H / 2 + 1);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_minimap(t_wolf *wolf)
{
	t_vec		loop;
	t_vec		coords;

	set_color(wolf, 1);
	loop.y = 0;
	while (loop.y < MINIMAP_H)
	{
		coords.y = wolf->pos_map.y + loop.y - (MINIMAP_H / 2);
		if (coords.y >= 0 && coords.y < wolf->map_h)
		{
			loop.x = 0;
			while (loop.x < MINIMAP_W)
			{
				coords.x = wolf->pos_map.x + loop.x - (MINIMAP_W / 2);
				if (coords.x >= 0 && coords.x < wolf->map_w)
					draw_point(wolf, coords, loop);
				loop.x++;
			}
		}
		loop.y++;
	}
	draw_center(wolf);
}
