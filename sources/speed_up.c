#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void		handle_speed_up(t_wolf *wolf, uint8_t type)
{
	if (!type)
		wolf->acc.y -= 0.5 * (GAME_MAX_SPEED * GAME_SPEED_UP_COEF
		- wolf->acc.y);
	else if (type == 1)
		wolf->acc.x += 0.5 * (GAME_MAX_SPEED * GAME_SPEED_UP_COEF
		- wolf->acc.x);
	else if (type == 2)
		wolf->acc.y += 0.5 * (GAME_MAX_SPEED * GAME_SPEED_UP_COEF
		- wolf->acc.y);
	else
		wolf->acc.x -= 0.5 * (GAME_MAX_SPEED * GAME_SPEED_UP_COEF
		- wolf->acc.x);
	wolf->speed_up = GAME_SPEED_UP_TIME;
}
