#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void	event_keys(t_wolf *wolf, int32_t key, uint8_t mode)
{
	if (key == SDLK_ESCAPE)
		exit(0);
	else if (key == SDLK_UP || key == SDLK_w)
		wolf->keys.up = mode;
	else if (key == SDLK_DOWN || key == SDLK_s)
		wolf->keys.down = mode;
	else if (key == SDLK_LEFT || key == SDLK_a)
		wolf->keys.left = mode;
	else if (key == SDLK_RIGHT || key == SDLK_d)
		wolf->keys.right = mode;
	else if (key == SDLK_SPACE)
		power_activate(wolf);
	if (key == SDLK_RETURN)
		if (mode)
		{
			wolf->keys.enter = !wolf->keys.enter;
			if (wolf->keys.enter)
				SDL_SetTextureAlphaMod(wolf->images.panel, 255);
			else
				SDL_SetTextureAlphaMod(wolf->images.panel, 200);
		}
}

static void	handle_event(t_wolf *wolf)
{
	SDL_Event	event;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			exit(0);
		else if (event.type == SDL_KEYDOWN)
			event_keys(wolf, event.key.keysym.sym, 1);
		else if (event.type == SDL_KEYUP)
			event_keys(wolf, event.key.keysym.sym, 0);
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void		loop(t_wolf *wolf)
{
	double		angle_rad;
	clock_t		time_before;
	clock_t		time_after;
	double		dt;
	double		frame_delay;
	SDL_Rect	rect;

	rect.x = 0;
	rect.y = 0;
	rect.w = WINDOW_W;
	rect.h = WINDOW_H / 2;
	frame_delay = 0;
	time_before = SDL_GetTicks();
	dt = 0;
	while (1)
	{
		handle_event(wolf);
		handle_movements(wolf, dt);
		handle_collisions(wolf);
		handle_crash(wolf);
		handle_power_charge(wolf, dt);
		SDL_SetRenderDrawColor(wolf->sdl.rend, 0, 0, 0, 0);
		SDL_RenderClear(wolf->sdl.rend);

		SDL_SetRenderDrawColor(wolf->sdl.rend, 0x2C, 0x0F, 0x45, 0);
		SDL_RenderFillRect(wolf->sdl.rend, &rect);

		angle_rad = wolf->angle * PI / 180.0;
		wolf->rot.x = cos(angle_rad);
		wolf->rot.y = sin(angle_rad);
		wolf->cam.x = cos(angle_rad + PI / 2) * wolf->cam_width;
		wolf->cam.y = sin(angle_rad + PI / 2) * wolf->cam_width;
		draw_map(wolf);
		draw_spaceship(wolf);
		draw_panel(wolf);
		draw_time(wolf);
		SDL_RenderPresent(wolf->sdl.rend);

		time_after = SDL_GetTicks();
		dt = ((double)(time_after - time_before)) / 1000.0;
		time_before = time_after;

		wolf->speed_up = MAX(0, wolf->speed_up - dt);
		wolf->time += dt;
		if ((frame_delay += dt) >= ANIM_DELAY)
		{
			if ((++(wolf->frame)) >= ANIM_MAX)
				wolf->frame = 0;
			frame_delay -= ANIM_DELAY;
		}
	}
}
