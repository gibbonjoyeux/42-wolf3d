#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			handle_crash(t_wolf *wolf)
{
	double		s_before;
	double		s_after;
	double		diff;

	s_before = sqrt(wolf->sacc.x * wolf->sacc.x + wolf->sacc.y * wolf->sacc.y);
	s_after = sqrt(wolf->acc.x * wolf->acc.x + wolf->acc.y * wolf->acc.y);
	diff = s_before - s_after;
	if (diff > GAME_CHOCK_MIN)
	{
		wolf->life -= diff;
		if (wolf->life < 0)
		{
			ct_putendl("You died. Shit man, take care of my spaceship...");
			exit(0);
		}
	}
}
