#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		get_floor_wall(t_dvec *floor_wall,
								t_vec map,
								double wall_x,
								uint8_t side)
{
	if (!side)
	{
		floor_wall->x = map.x;
		floor_wall->y = map.y + wall_x;
	}
	else if (side == 1)
	{
		floor_wall->x = map.x + wall_x;
		floor_wall->y = map.y + 1.0;
	}
	else if (side == 2)
	{
		floor_wall->x = map.x + 1.0;
		floor_wall->y = map.y + wall_x;
	}
	else
	{
		floor_wall->x = map.x + wall_x;
		floor_wall->y = map.y;
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_floor(t_wolf *wolf,
							t_vec map_coords,
							double dist_wall,
							double wall_x,
							uint8_t side,
							uint16_t x)
{
	uint16_t	y;
	t_dvec		floor_wall;
	t_dvec		floor_coords;
	double		dist;
	double		weight;
	uint8_t		tile;

	if ((y = WINDOW_H / 2 + (WINDOW_H / dist_wall) / 2) >= WINDOW_H)
		return ;
	get_floor_wall(&floor_wall, map_coords, wall_x, side);
	while (++y < WINDOW_H)
	{
		dist = WINDOW_H / (y * 2.0 - WINDOW_H);
		weight = dist / dist_wall;
		floor_coords.x = weight * floor_wall.x + (1.0 - weight) * wolf->pos.x;
		floor_coords.y = weight * floor_wall.y + (1.0 - weight) * wolf->pos.y;
		tile = wolf->map[(int32_t)floor_coords.y][(int32_t)floor_coords.x];
		if (tile >= 1 && tile <= 4)
			draw_floor_arrow(wolf, floor_coords, tile - 1, x, y);
		else
		{
			if (floor_coords.x - (uint32_t)floor_coords.x <= GRID_WIDTH
			|| floor_coords.y - (uint32_t)floor_coords.y <= GRID_WIDTH)
				SDL_RenderDrawPoint(wolf->sdl.rend, x, y);
		}
	}
}
