#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		move_left(char *name, uint8_t cursor)
{
	ct_memmove(name + cursor, name + cursor + 1, SCORE_NAME_MAX - cursor);
}

static void		move_right(char *name, uint8_t cursor)
{
	ct_memmove(name + cursor + 1, name + cursor, SCORE_NAME_MAX - cursor - 1);
}

static void		event_keys(t_wolf *wolf, int32_t key, char *name,
							uint8_t *cursor)
{
	if (key == SDLK_ESCAPE)
		exit(0);
	else if (key == SDLK_RETURN && name[0])
		wolf->keys.enter = 1;
	else if (key == SDLK_LEFT && *cursor > 0)
		*cursor -= 1;
	else if (key == SDLK_RIGHT && *cursor < SCORE_NAME_MAX && name[*cursor])
		*cursor += 1;
	else if (key == SDLK_BACKSPACE)
	{
		if (*cursor == 0)
			return ;
		*cursor -= 1;
		move_left(name, *cursor);
	}
	else if (key == SDLK_DELETE)
		move_left(name, *cursor);
	else if ((key >= 'a' && key <= 'z') || key == '-')
	{
		if (*cursor == SCORE_NAME_MAX)
			return ;
		move_right(name, *cursor);
		name[*cursor] = (ct_isalpha(key)) ? key : '-';
		*cursor += 1;
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			handle_score_events(t_wolf *wolf, char *name, uint8_t *cursor)
{
	SDL_Event	event;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			exit(0);
		else if (event.type == SDL_KEYDOWN)
			event_keys(wolf, event.key.keysym.sym, name, cursor);
	}
}
