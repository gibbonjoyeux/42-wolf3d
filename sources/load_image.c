#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

SDL_Texture		*load_image(t_wolf *wolf, char *path)
{
	SDL_Surface	*surface;
	SDL_Texture	*texture;

	surface = SDL_LoadBMP(path);
	if (!surface)
		return (NULL);
	texture = SDL_CreateTextureFromSurface(wolf->sdl.rend, surface);
	SDL_FreeSurface(surface);
	if (!texture)
	{
		SDL_FreeSurface(surface);
		return (NULL);
	}
	return (texture);
}
