#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_spaceship(t_wolf *wolf)
{
	SDL_Rect	pos;

	if (!wolf->keys.enter)
		return ;
	pos.x = 0;
	pos.y = 0;
	pos.w = WINDOW_W;
	pos.h = WINDOW_H;
	SDL_RenderCopy(wolf->sdl.rend, wolf->images.spaceship, NULL, &pos);
	draw_minimap(wolf);
}

/*
void			draw_spaceship(t_wolf *wolf)
{
	SDL_Rect	pos;
	SDL_Rect	part;

	if (!wolf->keys.enter)
		return ;
	pos.x = (WINDOW_W - 600) / 2;
	pos.y = WINDOW_H - 150;
	pos.w = 600;
	pos.h = 150;
	part.y = 0;
	part.w = 600;
	part.h = 150;
	if (wolf->angle_acc < -SPACESHIP_ROT_MIN * 2)
		part.x = 0;
	else if (wolf->angle_acc < -SPACESHIP_ROT_MIN)
		part.x = 600;
	else if (wolf->angle_acc > SPACESHIP_ROT_MIN * 2)
		part.x = 2400;
	else if (wolf->angle_acc > SPACESHIP_ROT_MIN)
		part.x = 1800;
	else
		part.x = 1200;
	SDL_RenderCopy(wolf->sdl.rend, wolf->images.spaceship, &part, &pos);
}
*/
