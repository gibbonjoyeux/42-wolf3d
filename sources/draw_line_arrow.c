#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static double	get_arrow_height(t_wolf *wolf,
									double height,
									double wall_precise,
									uint8_t side)
{
	if (side <= 1)
	{
		wall_precise -= (double)wolf->frame / ANIM_MAX;
		if (wall_precise < 0)
			wall_precise++;
		return (height * (1 - wall_precise));
	}
	else
	{
		wall_precise += (double)wolf->frame / ANIM_MAX;
		if (wall_precise > 1)
			wall_precise--;
		return (height - height * (1 - wall_precise));
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_line_arrow(t_wolf *wolf,
								double dist,
								uint16_t x,
								uint8_t type,
								uint8_t side,
								double wall_precise)
{
	double		coef;
	double		height;
	double		arrow_height;
	t_color		color_base;
	t_color		color_square;
	uint16_t	min_y;
	int32_t		y;

	coef = (side & 1) ? 0.5 : 1;
	coef = 1;
	get_color(&color_base, 1, coef);
	get_color(&color_square, 3, coef);
	height = WINDOW_H / dist;
	min_y = ABS((WINDOW_H - height) * 0.5);

	arrow_height = get_arrow_height(wolf, height, wall_precise,
	(side + type * 2) % 4);

	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_base.r, color_base.g,
	color_base.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, MAX(0, y), x,
	MIN(WINDOW_H, y + height));
	y = (WINDOW_H - arrow_height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_square.r, color_square.g,
	color_square.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + arrow_height);
}
