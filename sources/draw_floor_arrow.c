#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static uint8_t	draw_type_0(t_dvec tile_coords)
{
	if (tile_coords.x < 0.5)
	{
		if (tile_coords.y > 1 - tile_coords.x)
			return (1);
	}
	else
	{
		if (tile_coords.y > tile_coords.x)
			return (1);
	}
	return (0);

}

static uint8_t	draw_type_1(t_dvec tile_coords)
{
	if (tile_coords.y < 0.5)
	{
		if (tile_coords.x < tile_coords.y)
			return (1);
	}
	else
	{
		if (tile_coords.x < 1 - tile_coords.y)
			return (1);
	}
	return (0);
}

static uint8_t	draw_type_2(t_dvec tile_coords)
{
	if (tile_coords.x < 0.5)
	{
		if (tile_coords.y < tile_coords.x)
			return (1);
	}
	else
	{
		if (tile_coords.y < 1 - tile_coords.x)
			return (1);
	}
	return (0);
}

static uint8_t	draw_type_3(t_dvec tile_coords)
{
	if (tile_coords.y < 0.5)
	{
		if (tile_coords.x > 1 - tile_coords.y)
			return (1);
	}
	else
	{
		if (tile_coords.x > tile_coords.y)
			return (1);
	}
	return (0);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_floor_arrow(t_wolf *wolf,
									t_dvec floor_coords,
									uint8_t type,
									uint16_t x,
									uint16_t y)
{
	static	uint8_t	(*f_tab[4])(t_dvec) =
	{
		draw_type_0,
		draw_type_1,
		draw_type_2,
		draw_type_3
	};
	t_dvec		tile_coords;
	t_color		color;

	tile_coords.x = floor_coords.x - (uint32_t)floor_coords.x;
	tile_coords.y = floor_coords.y - (uint32_t)floor_coords.y;
	if (f_tab[type](tile_coords))
		get_color(&color, 1, 1);
	else
		get_color(&color, 3, 1);
	SDL_SetRenderDrawColor(wolf->sdl.rend, color.r, color.g, color.b, 0xFF);
	SDL_RenderDrawPoint(wolf->sdl.rend, x, y);
	SDL_SetRenderDrawColor(wolf->sdl.rend, 0x38, 0xDC, 0xFF, 0xFF);
}
