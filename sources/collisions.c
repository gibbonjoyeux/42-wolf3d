#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static uint8_t	is_round(double x)
{
	return (x == (int64_t)x);
}

static double	distance(double x, double y)
{
	return (x * x + y * y);
}

static void		load_min_max(t_wolf *wolf, t_vec *min, t_vec *max)
{
	min->x = wolf->pos.x;
	if (is_round(wolf->pos.x + GAME_PL_SIZE))
		max->x = (int32_t)(wolf->pos.x + GAME_PL_SIZE) - 1;
	else
		max->x = (int32_t)(wolf->pos.x + GAME_PL_SIZE);
	min->y = wolf->pos_map.y;
	if (is_round(wolf->pos.y + GAME_PL_SIZE))
		max->y = (int32_t)(wolf->pos.y + GAME_PL_SIZE) - 1;
	else
		max->y = (int32_t)(wolf->pos.y + GAME_PL_SIZE);
}

static void		load_tiles(t_wolf *wolf, uint8_t *tiles, t_vec pos)
{
	uint8_t		**map;

	map = wolf->map;
	if (pos.y <= wolf->map_h && pos.y > 0 && pos.x < wolf->map_w)
		tiles[0] = map[pos.y - 1][pos.x];
	if (pos.y <= wolf->map_h && pos.y > 0 && pos.x + 1 < wolf->map_w)
		tiles[1] = map[pos.y - 1][pos.x + 1];
	if (pos.y < wolf->map_h && pos.x + 1 < wolf->map_w)
		tiles[2] = map[pos.y][pos.x + 1];
	if (pos.y + 1 < wolf->map_h && pos.x + 1 < wolf->map_w)
		tiles[3] = map[pos.y + 1][pos.x + 1];
	if (pos.y + 1 < wolf->map_h && pos.x < wolf->map_w)
		tiles[4] = map[pos.y + 1][pos.x];
	if (pos.y + 1 < wolf->map_h && pos.x <= wolf->map_w)
		tiles[5] = map[pos.y + 1][pos.x - 1];
	if (pos.y < wolf->map_h && pos.x <= wolf->map_w)
		tiles[6] = map[pos.y][pos.x - 1];
	if (pos.y <= wolf->map_h && pos.y > 0 && pos.x <= wolf->map_w)
		tiles[7] = map[pos.y - 1][pos.x - 1];
}

static void		load_offsets(t_wolf *wolf, double *offsets, t_vec pos)
{
	offsets[0] = (double)pos.y - GAME_PL_SIZE - wolf->pos.y;
	offsets[1] = (double)pos.y + 1.0 - wolf->pos.y;
	offsets[2] = (double)pos.x - GAME_PL_SIZE - wolf->pos.x;
	offsets[3] = (double)pos.x + 1.0 - wolf->pos.x;
}

static int8_t	get_best(uint8_t *tiles, double *offsets)
{
	int8_t		i;
	int8_t		best;
	double		dist;
	double		best_dist;

	best = -1;
	i = -1;
	while (++i < 8)
	{
		if (i == 0)
			dist = distance(0, offsets[0]);
		else if (i == 1)
			dist = distance(offsets[3], offsets[0]);
		else if (i == 2)
			dist = distance(offsets[3], 0);
		else if (i == 3)
			dist = distance(offsets[3], offsets[1]);
		else if (i == 4)
			dist = distance(0, offsets[1]);
		else if (i == 5)
			dist = distance(offsets[2], offsets[1]);
		else if (i == 6)
			dist = distance(offsets[2], 0);
		else if (i == 7)
			dist = distance(offsets[2], offsets[0]);
		if (tiles[i] < GAME_TILE_SOLID && (best < 0 || (dist < best_dist)))
		{
			best = i;
			best_dist = dist;
		}
	}
	return (best);
}

static void		set_best(t_wolf *wolf, double *offsets, int8_t best)
{
	if (best == 0)
	{
		wolf->pos.y += offsets[0];
		wolf->acc.y = 0;
	}
	else if (best == 1)
	{
		wolf->pos.x += offsets[3];
		wolf->pos.y += offsets[0];
		wolf->acc.x = 0;
		wolf->acc.y = 0;
	}
	else if (best == 2)
	{
		wolf->pos.x += offsets[3];
		wolf->acc.x = 0;
	}
	else if (best == 3)
	{
		wolf->pos.x += offsets[3];
		wolf->pos.y += offsets[1];
		wolf->acc.x = 0;
		wolf->acc.y = 0;
	}
	else if (best == 4)
	{
		wolf->pos.y += offsets[1];
		wolf->acc.y = 0;
	}
	else if (best == 5)
	{
		wolf->pos.x += offsets[2];
		wolf->pos.y += offsets[1];
		wolf->acc.x = 0;
		wolf->acc.y = 0;
	}
	else if (best == 6)
	{
		wolf->pos.x += offsets[2];
		wolf->acc.x = 0;
	}
	else if (best == 7)
	{
		wolf->pos.x += offsets[2];
		wolf->pos.y += offsets[0];
		wolf->acc.x = 0;
		wolf->acc.y = 0;
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			handle_collisions(t_wolf *wolf)
{
	uint8_t		tiles[8];
	double		offsets[4];
	t_vec		pos;
	t_vec		min;
	t_vec		max;
	int8_t		best;

	wolf->sacc.x = wolf->acc.x;
	wolf->sacc.y = wolf->acc.y;
	wolf->pos.y -= GAME_PL_OFFSET;
	wolf->pos.x -= GAME_PL_OFFSET;
	wolf->pos_map.y = wolf->pos.y;
	wolf->pos_map.x = wolf->pos.x;
	load_min_max(wolf, &min, &max);
	pos.y = min.y;
	while (pos.y <= max.y)
	{
		pos.x = min.x;
		while (pos.x <= max.x)
		{
			if (pos.x < wolf->map_w && pos.y < wolf->map_h)
			{
				if (wolf->map[pos.y][pos.x] < GAME_TILE_SOLID)
				{
					if (wolf->map[pos.y][pos.x] > 0)
						handle_speed_up(wolf, wolf->map[pos.y][pos.x] - 1);
					++(pos.x);
					continue ;
				}
				else if (wolf->map[pos.y][pos.x] == GAME_TILE_END)
					loop_score(wolf);
			}
			ct_memset(tiles, GAME_TILE_SOLID, 8);
			load_tiles(wolf, tiles, pos);
			load_offsets(wolf, offsets, pos);
			if ((best = get_best(tiles, offsets)) >= 0)
				set_best(wolf, offsets, best);
			++(pos.x);
		}
		++(pos.y);
	}
	wolf->pos.y += GAME_PL_OFFSET;
	wolf->pos.x += GAME_PL_OFFSET;
	wolf->pos_map.y = wolf->pos.y;
	wolf->pos_map.x = wolf->pos.x;
}
