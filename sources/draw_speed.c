#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw(t_wolf *wolf, uint32_t speed)
{
	SDL_Rect	image_pos;
	SDL_Rect	image_part;
	uint32_t	x;
	char		*string;

	string = ct_itoa(speed);
	image_part.y = 0;
	image_part.w = 60;
	image_part.h = 60;
	image_pos.y = SPEED_Y;
	image_pos.w = 60;
	image_pos.h = 60;
	x = 0;
	while (string[x])
	{
		image_pos.x = SPEED_X + 30 * x;
		image_part.x = (string[x] - '0') * 60;
		SDL_RenderCopy(wolf->sdl.rend, wolf->images.digits, &image_part,
		&image_pos);
		++x;
	}
	free(string);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_speed(t_wolf *wolf)
{
	double		speed;

	speed = sqrt(wolf->sacc.x * wolf->sacc.x + wolf->sacc.y * wolf->sacc.y);
	draw(wolf, speed * 10);
}
