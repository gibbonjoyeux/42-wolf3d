#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		tile_error(char *tile)
{
	ct_mes_failure("Tiles not well formatted.");
	ct_printf("Tile: \"%s\"\n", tile);
	exit(1);
}

static char		is_line_empty(char *line)
{
	uint32_t	i;

	i = 0;
	while (line[i])
	{
		if (!ct_isspace(line[i]))
			return (0);
		i += 1;
	}
	return (1);
}

static char		**load_splitted(int32_t file)
{
	char		**split;
	char		*string;
	int32_t		ret;

	if ((ret = get_next_line(file, &string)) < 0)
		ct_exit_fail("An error occured while reading the map.");
	if (!ret)
		return (NULL);
	if (is_line_empty(string))
		return (NULL);
	if (!(split = ct_strsplitwhite(string)))
		ct_exit_fail("Not enough memory to load the map.");
	free(string);
	return (split);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

uint8_t			*load_map_line(int32_t file, uint16_t *length)
{
	char		**split;
	uint8_t		*line;
	uint16_t	i;

	if (!(split = load_splitted(file)))
		return (NULL);
	if (*length && *length != ct_strtablen(split))
		ct_exit_fail("Lines do not have the same length.");
	*length = ct_strtablen(split);
	if (!(line = calloc(*length + 2, sizeof(uint8_t))))
		ct_exit_fail("Not enough memory to load the map.");
	line[0] = GAME_TILE_SOLID;
	line[*length + 1] = GAME_TILE_SOLID;
	i = 0;
	while (i < *length)
	{
		line[i + 1] = ct_atoi(split[i]);
		if (!line[i + 1] && !ct_isdigit(split[i][0]))
			tile_error(split[i]);
		free(split[i]);
		++i;
	}
	free(split);
	return (line);
}
