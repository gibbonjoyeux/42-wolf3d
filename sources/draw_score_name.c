#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw_cursor(t_wolf *wolf, uint8_t cursor)
{
	SDL_Rect	img_pos;
	SDL_Rect	img_part;

	if (cursor == SCORE_NAME_MAX)
		return ;
	img_pos.x = SCORE_NAME_X + cursor * SCORE_NAME_DIST;
	img_pos.y = SCORE_CURSOR_Y;
	img_pos.w = 60;
	img_pos.h = 5;
	img_part.x = 0;
	img_part.y = 0;
	img_part.w = 60;
	img_part.h = 5;
	SDL_RenderCopy(wolf->sdl.rend, wolf->images.cursor, &img_part, &img_pos);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_score_name(t_wolf *wolf, char *name, uint8_t cursor)
{
	SDL_Rect	img_pos;
	SDL_Rect	img_part;
	uint8_t		i;

	img_pos.y = SCORE_NAME_Y;
	img_pos.w = 60;
	img_pos.h = 60;
	img_part.y = 0;
	img_part.w = 60;
	img_part.h = 60;
	i = 0;
	while (name[i])
	{
		img_pos.x = SCORE_NAME_X + i * SCORE_NAME_DIST;
		img_part.x = ((ct_isalpha(name[i])) ? name[i] - 'a' : 26) * 60;
		SDL_RenderCopy(wolf->sdl.rend, wolf->images.alpha, &img_part,
		&img_pos);
		i += 1;
	}
	draw_cursor(wolf, cursor);
}
