#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void	handle_speed_limits(t_wolf *wolf, uint8_t accelerating)
{
	double	speed;
	double	max_speed;
	double	slow_coef;

	speed = sqrt(wolf->acc.x * wolf->acc.x + wolf->acc.y * wolf->acc.y);
	if (wolf->speed_up > 0)
		max_speed = GAME_MAX_SPEED * (1 + (wolf->speed_up / GAME_SPEED_UP_TIME)
		* GAME_SPEED_UP_COEF);
	else
		max_speed = GAME_MAX_SPEED;
	if (speed > max_speed)
	{
		slow_coef = speed / max_speed;
		wolf->acc.x /= slow_coef;
		wolf->acc.y /= slow_coef;
	}
	else if (!accelerating && speed < GAME_MIN_SPEED)
	{
		wolf->acc.x = 0;
		wolf->acc.y = 0;
	}
}

static void	handle_rotation(t_wolf *wolf, double dt)
{
	if (wolf->keys.left)
		wolf->angle_acc -= GAME_ANGLE_ACC * dt;
	else if (wolf->keys.right)
		wolf->angle_acc += GAME_ANGLE_ACC * dt;
	else
	{
		if (wolf->angle_acc > GAME_ANGLE_MIN_SPEED)
			wolf->angle_acc -= GAME_ANGLE_SLOW * dt;
		else if (wolf->angle_acc < -GAME_ANGLE_MIN_SPEED)
			wolf->angle_acc += GAME_ANGLE_SLOW * dt;
		else
			wolf->angle_acc = 0;
	}
	wolf->angle_acc = MAX(-GAME_ANGLE_MAX_SPEED, MIN(GAME_ANGLE_MAX_SPEED,
	wolf->angle_acc));
	wolf->angle += wolf->angle_acc * dt;
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void		handle_movements(t_wolf *wolf, double dt)
{
	uint8_t	accelerating;
	double	speed;

	accelerating = wolf->keys.up | wolf->keys.down;
	speed = sqrt(wolf->acc.x * wolf->acc.x + wolf->acc.y * wolf->acc.y);
	handle_rotation(wolf, dt);
	if (wolf->keys.up)
	{
		wolf->acc.x += cos(wolf->angle * PI / 180.0) * GAME_ACC * dt;
		wolf->acc.y += sin(wolf->angle * PI / 180.0) * GAME_ACC * dt;
	}
	else if (wolf->keys.down)
	{
		wolf->acc.x -= cos(wolf->angle * PI / 180.0) * GAME_ACC * dt;
		wolf->acc.y -= sin(wolf->angle * PI / 180.0) * GAME_ACC * dt;
	}
	if (!accelerating)
	{
		wolf->acc.x -= wolf->acc.x * GAME_SLOW * dt;
		wolf->acc.y -= wolf->acc.y * GAME_SLOW * dt;
	}
	handle_speed_limits(wolf, accelerating);
	wolf->pos.x += wolf->acc.x * dt;
	wolf->pos.y += wolf->acc.y * dt;
	wolf->pos.x = MAX(0, MIN(wolf->map_w, wolf->pos.x));
	wolf->pos.y = MAX(0, MIN(wolf->map_h, wolf->pos.y));
	wolf->pos_map.x = (int32_t)wolf->pos.x;
	wolf->pos_map.y = (int32_t)wolf->pos.y;
	wolf->speed = sqrt(wolf->acc.x * wolf->acc.x + wolf->acc.y * wolf->acc.y);
	wolf->cam_width = DEFAULT_CAM_WIDTH + (speed / GAME_MAX_SPEED)
	* GAME_CAMSPEED_COEF;
}
