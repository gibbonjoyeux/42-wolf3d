#include "wolf.h"

#define		BLOCK_START		0
#define		BLOCK_MIDDLE	1
#define		BLOCK_END		2

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void inline	draw_block(t_wolf *wolf, uint8_t type, uint16_t x)
{
	SDL_Rect		img_pos;
	SDL_Rect		img_part;
	SDL_Texture		*image;

	img_pos.x = x;
	img_pos.y = POWERBAR_Y;
	img_pos.w = POWERBAR_UNIT_W;
	img_pos.h = 13;
	img_part.x = type * POWERBAR_UNIT_W;
	img_part.y = 0;
	img_part.w = POWERBAR_UNIT_W;
	img_part.h = 13;
	if (wolf->power_charge == GAME_POWER_MAX)
		image = wolf->images.powerbarmax;
	else
		image = wolf->images.powerbar;
	SDL_RenderCopy(wolf->sdl.rend, image, &img_part, &img_pos);
}

static void			draw_main_even(t_wolf *wolf, uint8_t to_draw, uint16_t offset)
{
	draw_block(wolf, BLOCK_MIDDLE, POWERBAR_X - offset);
	draw_block(wolf, BLOCK_MIDDLE, POWERBAR_X + offset);
	if (to_draw > 0)
		draw_main_even(wolf, to_draw - 2, offset + POWERBAR_UNIT_W);
}

static void			draw_main_odd(t_wolf *wolf, uint8_t to_draw)
{
	draw_block(wolf, BLOCK_MIDDLE, POWERBAR_X);
	if (to_draw > 0)
		draw_main_even(wolf, to_draw - 1, 5);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void				draw_powerbar(t_wolf *wolf)
{
	uint8_t			block_number;

	block_number =
	((wolf->power_charge / GAME_POWER_MAX) * POWERBAR_W) / POWERBAR_UNIT_W;
	if (block_number & 1)
	{
		draw_main_odd(wolf, block_number);
		draw_block(wolf, BLOCK_START,
		POWERBAR_X - (block_number / 2) * POWERBAR_UNIT_W - 15);
		draw_block(wolf, BLOCK_END,
		POWERBAR_X + (block_number / 2) * POWERBAR_UNIT_W + 15);
	}
	else
	{
		draw_main_even(wolf, block_number, 0);
		draw_block(wolf, BLOCK_START,
		POWERBAR_X - (block_number / 2) * POWERBAR_UNIT_W - POWERBAR_UNIT_W);
		draw_block(wolf, BLOCK_END,
		POWERBAR_X + (block_number / 2) * POWERBAR_UNIT_W + POWERBAR_UNIT_W);
	}
}
