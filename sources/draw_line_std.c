#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void		draw_line_std(t_wolf *wolf, double dist, uint16_t x, uint8_t type,
							uint8_t side)
{
	double		height;
	uint16_t	y;
	t_color		color;

	get_color(&color, side, 1);//(side & 1) ? 0.5 : 1);
	--type;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color.r, color.g, color.b, 0xFF);
	if ((height = WINDOW_H / dist) < WINDOW_H)
	{
		y = (WINDOW_H - height) / 2;
		SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + height);
	}
	else
		SDL_RenderDrawLine(wolf->sdl.rend, x, 0, x, WINDOW_H);
}
