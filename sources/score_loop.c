#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

//static void		print_scores(t_score *scores)
//{
//	uint8_t		i;
//
//	i = 0;
//	while (i < SCORE_MAX_SAVE && scores[i].score)
//	{
//		write(1, scores[i].name, SCORE_NAME_MAX);
//		ct_putchar(' ');
//		ct_putuint32(scores[i].score);
//		ct_putchar('\n');
//		i += 1;
//	}
//}

static void		save_new_score(t_wolf *wolf, t_score *scores,
				uint8_t score_index, char *name)
{
	score_save(wolf, scores, score_index, name);
	SDL_SetRenderDrawColor(wolf->sdl.rend, 0, 0, 0, 0);
	SDL_RenderClear(wolf->sdl.rend);
	draw_scores(wolf, scores);
	SDL_RenderPresent(wolf->sdl.rend);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			loop_score(t_wolf *wolf)
{
	t_score		scores[SCORE_MAX_SAVE];
	char		name[SCORE_NAME_MAX + 1];
	uint8_t		cursor;
	uint8_t		score_index;

	wolf->score = wolf->time * 1000;
	wolf->keys.enter = 0;
	load_scores(wolf, scores, &score_index);
	ct_memset(name, 0, SCORE_NAME_MAX);
	cursor = 0;
	SDL_SetRenderDrawColor(wolf->sdl.rend, 0, 0, 0, 0);
	while (1)
	{
		SDL_RenderClear(wolf->sdl.rend);
		handle_score_events(wolf, name, &cursor);
		draw_score_name(wolf, name, cursor);
		draw_new_score(wolf);
		draw_scores(wolf, scores);
		SDL_RenderPresent(wolf->sdl.rend);
		if (wolf->keys.enter)
		{
			save_new_score(wolf, scores, score_index, name);
			sleep(2);
			exit(0);
		}
	}
}
