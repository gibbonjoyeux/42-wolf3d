#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static inline uint8_t	get_cube_side(t_vec step, uint8_t side)
{
	if (side)
		return ((step.y >= 0) ? 3 : 1 );
	else
		return ((step.x >= 0) ? 0 : 2 );
}

static inline void		draw_ray(t_wolf *wolf, t_dvec ray, uint16_t x)
{
	t_dvec				delta_dist;
	t_dvec				side_dist;
	double				wall_dist;
	t_vec				map;
	t_vec				step;
	uint8_t				side;
	double				wall_precise;

	map.x = wolf->pos_map.x;
	map.y = wolf->pos_map.y;
	delta_dist.x = ABS(1.0 / ray.x);
	delta_dist.y = ABS(1.0 / ray.y);
	step.x = (ray.x < 0) ? -1 : 1;
	step.y = (ray.y < 0) ? -1 : 1;
	side_dist.x = delta_dist.x * ((ray.x < 0) ? wolf->pos.x - map.x : map.x
	- wolf->pos.x + 1);
	side_dist.y = delta_dist.y * ((ray.y < 0) ? wolf->pos.y - map.y : map.y
	- wolf->pos.y + 1);
	while (1)
	{
		if (side_dist.x < side_dist.y)
		{
			side_dist.x += delta_dist.x;
			map.x += step.x;
			side = 0;
		}
		else
		{
			side_dist.y += delta_dist.y;
			map.y += step.y;
			side = 1;
		}
		if (wolf->map[map.y][map.x] > 4)
			break ;
	}
	if (!side)
	{
		wall_dist = (map.x - wolf->pos.x + (1 - step.x) / 2) / ray.x;
		wall_precise = wolf->pos.y + wall_dist * ray.y;
	}
	else
	{
		wall_dist = (map.y - wolf->pos.y + (1 - step.y) / 2) / ray.y;
		wall_precise = wolf->pos.x + wall_dist * ray.x;
	}
	wall_precise -= (int32_t)wall_precise;
	side = get_cube_side(step, side);

	if (wolf->map[map.y][map.x] <= 4)
		return ;
	if (wolf->map[map.y][map.x] == 5)
		draw_line_std(wolf, wall_dist, x, wolf->map[map.y][map.x], side);
	else if (wolf->map[map.y][map.x] <= 7)
		draw_line_arrow(wolf, wall_dist, x, wolf->map[map.y][map.x] - 2, side,
		wall_precise);
	else if (wolf->map[map.y][map.x] == 8)
		draw_line_square(wolf, wall_dist, x, wall_precise);
	else if (wolf->map[map.y][map.x] == 9)
		draw_line_speed(wolf, wall_dist, x);
	else if (wolf->map[map.y][map.x] <= 24)
		draw_line_rect(wolf, wall_dist, x, wolf->map[map.y][map.x] - 5, side,
		wall_precise);
	else if (wolf->map[map.y][map.x] == 25)
		draw_line_end(wolf, wall_dist, x);
	//SDL_SetRenderDrawColor(wolf->sdl.rend, 0xFF, 0x1F, 0x80, 0xFF);
	SDL_SetRenderDrawColor(wolf->sdl.rend, 0x38, 0xDC, 0xFF, 0xFF);
	draw_floor(wolf, map, wall_dist, wall_precise, side, x);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void				draw_map(t_wolf *wolf)
{
	uint16_t		x;
	double			win_x;
	t_dvec			ray;

	//wolf->pos_map.x = (int32_t)wolf->pos.x;
	//wolf->pos_map.y = (int32_t)wolf->pos.y;
	x = 0;
	while (x < WINDOW_W)
	{
		win_x = 2 * ((double)x / WINDOW_W) - 1;
		ray.x = wolf->rot.x + wolf->cam.x * win_x;
		ray.y = wolf->rot.y + wolf->cam.y * win_x;
		draw_ray(wolf, ray, x);
		++x;
	}
}
